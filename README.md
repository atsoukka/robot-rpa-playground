# Robot Framework Camunda RPA playground

This project provides a playground for safely trying out a Robot Framework based open source RPA solution with Camunda Platform.


## Why open source RPA?

Robot process automation (RPA) is an umbrella term for technologies and methods used to automate or integrate systems, which were not designed to be automated or integrated otherwise. That's why RPA is usually related to legacy systems only.

But once you have the infrastructure for executing RPA bots in a scalable manner, any problem could be seen as an RPA problem, and the RPA system eventually becomes a FaaS (Function as a Service) solution. Or could become, unless inhibited by price of the selected RPA solution...


## Quick start

Check out this repository and build and start everything with [Docker Compose](https://docs.docker.com/compose/):

```bash
$ git clone https://gitlab.com/atsoukka/robot-rpa-playground.git
$ cd robot-rpa-playground
$ docker-compose up
```

Copy a Jupyter Lab URL with token from the container log:

```bash
$ docker-compose logs -f robotlab
```

It is the last URL in the log output that looks like:

```text
robotlab_1  |     To access the server, open this file in a browser:
robotlab_1  |         file:///root/.local/share/jupyter/runtime/jpserver-1-open.html
robotlab_1  |     Or copy and paste one of these URLs:
robotlab_1  |         http://67cacae9a154:8888/lab?token=69910d00caf3e20d45730b8834aa14f68e812ffe9a6deb68
robotlab_1  |         http://127.0.0.1:8888/lab?token=69910d00caf3e20d45730b8834aa14f68e812ffe9a6deb68
```

Now you should be able to access all the services provided by this playground:

* http://localhost:8080/ – Camunda Platform with deployed example process (username and password: `demo`)
* http://127.0.0.1:8888/lab?token= – Jupyter Lab (for full URL see instructions above)
* http://localhost:8889/ – for noVNC view on Selenium-driven Firefox used by the example bot.

At Jupyter Lab, open `worker.py` and `xkcd.robot` from context menu (by right mouse click) option *Open With --> Notebook*.


## Visual tour

![](./docs/rpa-playground-camunda-01.png)

At Camunda Tasklist (http://localhost:8080/) choose *Start process* and *XKCD Comic search*.

![](./docs/rpa-playground-camunda-02.png)

Next on the process start from, submit some search form for the comic search.

![](./docs/rpa-playground-camunda-03.png)

Now at Camunda Cockpit, a new process should have been started, and it should have been stopped at external task to wait for a robot to work on the task.

![](./docs/rpa-playground-worker-01.png)

Then open Jupyter Lab and `worker.py` using *Open With --> Notebook* -feature via its file browser context menu.

![](./docs/rpa-playground-worker-02.png)

Execute the opened notebook to get your own interactive RPA execution dashboard for the example robot.

![](./docs/rpa-playground-worker-03.png)

At first, choose task *xkcd.robot: Search for XKCD image* and execute it. Then choose the other one to execute tasks queued for it. Since one execution completes only one queued external task for the chosen topic, you may want to check Camunda Cockpit for how may tasks there are left.

![](./docs/rpa-playground-novnc-01.png)

Before trying this again, try to get visual on the Selenium-driven Firefox in use by opening http://localhost:8889/ and connecting.

![](./docs/rpa-playground-camunda-04.png)

Finally, there should be human tasks waiting at Camunda Tasklist with data fetched by your robots.


## More information

Robot Framework RPA is already in wide used outside Camunda. Please, [check the forums fore more information…](https://forum.robotframework.org/c/rpa/29)


## Troubleshooting

* When executing RPA bot using `worker.py`, I see:

  ```text
  ==============================================================================
  Xkcd
  ==============================================================================
  Search for XKCD image
  ```

  but nothing happens. Also, I don't see browser opening at connected noVNC session
  at http://localhost:8889/.

  Try restarting the Selenium (Firefox) -container:

  ```bash
  docker-compose restart selenium
  ```

  and running the bot again.


## Acknowledgements

The example process uses content from https://xkcd.com/
