.PHONY: all
all: build run

.PHONY: build
build:
	docker-compose -f docker-compose-build.yml build

.PHONY: run
run:
	docker-compose -f docker-compose-build.yml up

.PHONY: format
format:
	nix-shell -p python3Packages.black --run "black robotlab/src"
	nix-shell -p python3Packages.isort --run "isort -rc robotlab/src"
