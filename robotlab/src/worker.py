# # RPA worker dashboard

# Execute the cell below, for RPA task execution form:

# +
from IPython.display import clear_output
from IPython.display import HTML
from ipywidgets import widgets
from robot.model import SuiteVisitor
from robot.running import TestSuiteBuilder
from tempfile import TemporaryDirectory
import os


tasks = []


class TaskFinder(SuiteVisitor):
    def __init__(self, suite):
        super(TaskFinder, self).__init__()
        self.suite = suite

    def visit_test(self, test):
        tasks.append(((f"{self.suite}: {test.name}", [self.suite, test.name])))


TestSuiteBuilder().build("xkcd.robot").visit(TaskFinder("xkcd.robot"))

options = widgets.SelectMultiple(options=tasks, rows=10)
button = widgets.Button(description="Run robot, run!")
output = widgets.Output()


def run(b):
    cwd = os.getcwd()
    try:
        button.disabled = True
        with TemporaryDirectory() as tmpdir:
            os.chdir(tmpdir)
            with output:
                clear_output(wait=True)
                for value in options.value:
                    # !PYTHONPATH={cwd} robot --rpa --exitonfailure --listener CamundaListener -l NONE -r NONE -t "{value[1]}" {cwd}/{value[0]}
    finally:
        os.chdir(cwd)
        button.disabled = False


button.on_click(run)

display(HTML("<h1>Choose RPA tasks to run</h1>"))
display(options)
display(button, output)
# -

# This dashboard can be used to try out Robot Framework RPA for a single Camunda external task at time. For automatic scheduling, there exists [better](https://datakurre.pandala.org/2021/04/camunda-nomad-robotframework-rpa/) [alternatives](https://robocorp.com/).
