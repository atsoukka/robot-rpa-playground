# # XKCD comic search RPA worker

# This is an example of combined RPA suite for both external tasks in XKCD comic search example process.

# Executing this worker directly from Notebook, will fetch, lock and update, but not complete the task at Camunda. For completing the task, with executio logs being submitted to Camunda, use the RPA worker dashboard available in `worker.py`.

# +
*** Settings ***

Library  SeleniumLibrary  timeout=5s
...      plugins=SeleniumTestability;True;5s;True
Library  SeleniumScreenshots
Library  Camunda
Library  Collections
Library  OperatingSystem
Library  requests

Task setup  Fetch and lock external task  ${TEST_NAME}  timeout=1 s  lock=1 s

# +
*** Keywords ***

Open task browser
    [Documentation]
    ...  Open one empty browser, and keep using that on subsequent calls.
    Open browser  about:blank
    ...  alias=default  browser=firefox  remote_url=http://selenium:4444/wd/hub

# +
*** Keywords ***

Run Selenium keyword and return status
    [Documentation]
    ...  Run Selenium keyword (optionally with arguments)
    ...  and return status without screenshots on failure
    [Arguments]  ${keyword}  @{arguments}
    ${tmp}=  Register keyword to run on failure  No operation
    ${status}=  Run keyword and return status  ${keyword}  @{arguments}
    Register keyword to run on failure  ${tmp}
    [Return]  ${status}

# +
*** Tasks ***

Search for XKCD image
    ${query}=  Get external task variable  query

    Open task browser
    Go to  https://www.google.com/search?q=site%3Am.xkcd.com+${query}
    
    ${has modal}=  Run Selenium keyword and return status
    ...  Page should contain element  css:[aria-modal="true"]

    Run keyword if  ${has modal} == True
    ...  Remove element  css:[aria-modal="true"]

    ${has results}=  Run Selenium keyword and return status
    ...  Page should contain element
    ...  xpath://a[starts-with(@href, "https://m.xkcd.com/")]

    Run keyword if  ${has results} == False
    ...  Set external task BPMN error  not_found  No results found.

    Capture page screenshot

    ${count}=  Get Element Count  xpath://a[starts-with(@href, "https://m.xkcd.com/")]
    ${results}=  Create list
    FOR  ${index}  IN RANGE  ${count}
        ${href}=  Get Element Attribute
        ...  xpath:(//a[starts-with(@href, "https://m.xkcd.com/")])[${{${index} + 1}}]
        ...  href
        Append to list  ${results}  ${href}
    END
    Set external task variable  results  ${results}  type=Json

# +
*** Tasks ***

Download XKCD image
    ${url}=  Get external task variable  url

    Open task browser
    Go to  ${url}
    Capture page screenshot

    ${has image}=  Run Selenium keyword and return status
    ...  Page should contain element
    ...  css:#comic img

    Run keyword if  ${has image} == False
    ...  Set external task BPMN error  not_found  Image not found.

    ${alt}=  Get Element Attribute  css:#comic img  alt
    ${title}=  Get Element Attribute  css:#comic img  title
    ${src}=  Get Element Attribute  css:#comic img  src
    Set external task variable  imageUrl  ${src}  type=String

    ${response}  Get  ${src}
    Create binary file  comic.png  ${response.content}
    Set external task variable  comic  comic.png  type=File
    Set external task variable  alt  ${alt}  type=String
    Set external task variable  title  ${title}  type=String

# +
*** Settings ***

Suite teardown  Close all browsers
